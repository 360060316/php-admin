-- 主机： 127.0.0.1
-- 生成日期： 2023-05-21 12:10:36
-- Mysql 版本： 10.4.28-MariaDB
-- PHP 版本： 8.2.4

--
-- 数据库： `admin`
--

SET FOREIGN_KEY_CHECKS = 0;

--
-- 表 `pan_address` 结构
--

DROP TABLE IF EXISTS `pan_address`;

CREATE TABLE `pan_address` (`id` int(11) NOT NULL AUTO_INCREMENT,`url` varchar(1000) NOT NULL,`code` varchar(1000) DEFAULT NULL,`message` varchar(1000) DEFAULT NULL,`status` tinyint(1) DEFAULT -1,`atime` timestamp NULL DEFAULT current_timestamp(),`mtime` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 表 `pan_address` 数据
--

--
-- 表 `pan_admin` 结构
--

DROP TABLE IF EXISTS `pan_admin`;

CREATE TABLE `pan_admin` (`id` int(11) NOT NULL AUTO_INCREMENT,`role` varchar(100) NOT NULL,`user` varchar(100) NOT NULL,`pswd` varchar(100) NOT NULL,`avatar` varchar(1000) DEFAULT NULL,`phone` varchar(100) DEFAULT NULL,`mail` varchar(100) DEFAULT NULL,`status` tinyint(1) DEFAULT 0,`dtime` timestamp NULL DEFAULT NULL,`atime` timestamp NULL DEFAULT current_timestamp(),`mtime` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 表 `pan_admin` 数据
--
INSERT INTO `pan_admin` SET `id` = 4 , `role` = '[1]' , `user` = '360060316' , `pswd` = 'c4fe79ba4824fb2f55142cdcd7864c94' , `avatar` = '' , `phone` = '' , `mail` = '' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 20:40:06' , `mtime` = '2023-05-18 14:16:31';
INSERT INTO `pan_admin` SET `id` = 5 , `role` = '[2]' , `user` = 'admin' , `pswd` = 'f6c57d7dd86b31022a8924036c113a75' , `avatar` = '' , `phone` = '' , `mail` = '' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 14:16:27' , `mtime` = '2023-05-18 14:16:27';

--
-- 表 `pan_log` 结构
--

DROP TABLE IF EXISTS `pan_log`;

CREATE TABLE `pan_log` (`id` int(11) NOT NULL AUTO_INCREMENT,`uid` int(11) DEFAULT 0,`module` varchar(100) DEFAULT NULL,`controller` varchar(100) DEFAULT NULL,`action` varchar(100) DEFAULT NULL,`method` varchar(100) DEFAULT NULL,`type` varchar(100) DEFAULT NULL,`url` varchar(1000) DEFAULT NULL,`ip` varchar(100) DEFAULT NULL,`param` longtext DEFAULT NULL,`agent` varchar(1000) DEFAULT NULL,`referer` varchar(1000) DEFAULT NULL,`result` longtext DEFAULT NULL,`atime` timestamp NULL DEFAULT current_timestamp(),PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 表 `pan_log` 数据
--

--
-- 表 `pan_menu` 结构
--

DROP TABLE IF EXISTS `pan_menu`;

CREATE TABLE `pan_menu` (`id` int(11) NOT NULL AUTO_INCREMENT,`pid` int(11) DEFAULT 0,`name` varchar(100) NOT NULL,`title` varchar(100) DEFAULT NULL,`alias` varchar(100) DEFAULT NULL,`icon` varchar(100) DEFAULT NULL,`path` varchar(100) NOT NULL,`redirect` varchar(100) DEFAULT NULL,`component` varchar(100) DEFAULT NULL,`aside` tinyint(1) DEFAULT 1,`breadcrumb` tinyint(1) DEFAULT 1,`label` tinyint(1) DEFAULT 1,`cache` tinyint(1) DEFAULT 0,`close` tinyint(1) DEFAULT 0,`props` tinyint(1) DEFAULT 0,`sort` tinyint(1) DEFAULT 0,`status` tinyint(1) DEFAULT 0,`dtime` timestamp NULL DEFAULT NULL,`atime` timestamp NULL DEFAULT current_timestamp(),`mtime` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 表 `pan_menu` 数据
--
INSERT INTO `pan_menu` SET `id` = 1 , `pid` = 0 , `name` = 'Home' , `title` = '主页' , `alias` = '' , `icon` = 'House' , `path` = '/' , `redirect` = '/console' , `component` = 'Layout' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 0 , `props` = 0 , `sort` = 0 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-14 14:13:31' , `mtime` = '2023-05-17 21:57:41';
INSERT INTO `pan_menu` SET `id` = 2 , `pid` = 1 , `name` = 'Console' , `title` = '控制台' , `alias` = '' , `icon` = '' , `path` = '/console' , `redirect` = NULL , `component` = '/src/views/console/index.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 0 , `props` = 0 , `sort` = 0 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-14 16:59:46' , `mtime` = '2023-05-14 23:50:29';
INSERT INTO `pan_menu` SET `id` = 5 , `pid` = 4 , `name` = 'Account' , `title` = '账户管理' , `alias` = '' , `icon` = '' , `path` = '/system/account' , `redirect` = NULL , `component` = '/src/views/system/account.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 1 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-14 20:00:36' , `mtime` = '2023-05-15 14:36:10';
INSERT INTO `pan_menu` SET `id` = 4 , `pid` = 0 , `name` = 'System' , `title` = '系统设置' , `alias` = '' , `icon` = 'Setting' , `path` = '/system' , `redirect` = '/system/account' , `component` = 'Layout' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 2 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-14 19:06:46' , `mtime` = '2023-05-18 20:44:15';
INSERT INTO `pan_menu` SET `id` = 6 , `pid` = 4 , `name` = 'Role' , `title` = '角色管理' , `alias` = '' , `icon` = '' , `path` = '/system/role' , `redirect` = NULL , `component` = '/src/views/system/role.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 2 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-14 20:04:00' , `mtime` = '2023-05-15 14:36:14';
INSERT INTO `pan_menu` SET `id` = 7 , `pid` = 4 , `name` = 'Menu' , `title` = '菜单管理' , `alias` = '' , `icon` = '' , `path` = '/system/menu' , `redirect` = NULL , `component` = '/src/views/system/menu.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 3 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-14 20:04:21' , `mtime` = '2023-05-15 14:36:21';
INSERT INTO `pan_menu` SET `id` = 9 , `pid` = 0 , `name` = 'Iptv' , `title` = 'IPTV' , `alias` = '' , `icon` = 'Monitor' , `path` = '/iptv' , `redirect` = '' , `component` = 'Layout' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 1 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 12:22:53' , `mtime` = '2023-05-19 19:40:55';
INSERT INTO `pan_menu` SET `id` = 10 , `pid` = 9 , `name` = 'Channel' , `title` = '频道管理' , `alias` = '' , `icon` = '' , `path` = '/iptv/' , `redirect` = '' , `component` = '/src/views/iptv/index.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 0 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 13:29:31' , `mtime` = '2023-05-18 20:48:44';
INSERT INTO `pan_menu` SET `id` = 11 , `pid` = 4 , `name` = 'Log' , `title` = '系统日志' , `alias` = '' , `icon` = '' , `path` = '/system/log' , `redirect` = '' , `component` = '/src/views/system/log.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 5 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 14:33:48' , `mtime` = '2023-05-15 14:36:25';
INSERT INTO `pan_menu` SET `id` = 12 , `pid` = 4 , `name` = 'Config' , `title` = '系统配置' , `alias` = '' , `icon` = '' , `path` = '/system/config' , `redirect` = '' , `component` = '/src/views/system/config.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 0 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 14:35:55' , `mtime` = '2023-05-15 14:35:55';
INSERT INTO `pan_menu` SET `id` = 13 , `pid` = 4 , `name` = 'Database' , `title` = '数据库管理' , `alias` = '' , `icon` = '' , `path` = '/system/database' , `redirect` = '' , `component` = '/src/views/system/database.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 4 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 14:36:57' , `mtime` = '2023-05-15 14:37:29';
INSERT INTO `pan_menu` SET `id` = 14 , `pid` = 12 , `name` = 'Upload' , `title` = '上传配置' , `alias` = '' , `icon` = '' , `path` = '/system/menu/upload' , `redirect` = '' , `component` = '/src/views/system/upload.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 0 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 14:41:50' , `mtime` = '2023-05-15 14:42:42';
INSERT INTO `pan_menu` SET `id` = 15 , `pid` = 9 , `name` = 'Scan' , `title` = '频道扫描' , `alias` = '' , `icon` = '' , `path` = '/iptv/scan' , `redirect` = '' , `component` = '/src/views/iptv/scan.vue' , `aside` = 1 , `breadcrumb` = 1 , `label` = 1 , `cache` = 0 , `close` = 1 , `props` = 0 , `sort` = 0 , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 09:57:52' , `mtime` = '2023-05-18 20:50:02';

--
-- 表 `pan_result` 结构
--

DROP TABLE IF EXISTS `pan_result`;

CREATE TABLE `pan_result` (`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(100) NOT NULL,`url` varchar(1000) NOT NULL,`video_width` varchar(100) DEFAULT NULL,`video_height` varchar(100) DEFAULT NULL,`video_encoded` varchar(100) DEFAULT NULL,`video_frame` varchar(100) DEFAULT NULL,`video_rate` varchar(100) DEFAULT NULL,`audio_encoded` varchar(100) DEFAULT NULL,`audio_track` varchar(100) DEFAULT NULL,`audio_sampling_rate` int(11) DEFAULT NULL,PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 表 `pan_result` 数据
--

--
-- 表 `pan_role` 结构
--

DROP TABLE IF EXISTS `pan_role`;

CREATE TABLE `pan_role` (`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(100) NOT NULL,`title` varchar(100) NOT NULL,`menu` varchar(100) DEFAULT '[]',`action` varchar(100) DEFAULT '[]',`role` varchar(100) DEFAULT '[]',`status` tinyint(1) DEFAULT 0,`dtime` timestamp NULL DEFAULT NULL,`atime` timestamp NULL DEFAULT current_timestamp(),`mtime` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 表 `pan_role` 数据
--
INSERT INTO `pan_role` SET `id` = 1 , `name` = 'SUPER_ADMIN' , `title` = '超级管理员' , `menu` = '[1,2,4,12,14,5,6,7,13,11,9,10,15]' , `action` = '[30,27,29,32,28,31,22,25,5,4,2,16,20,18,13,14,17,15,19,10,12,33,11,21,24,26,8,3,23,7,6,1,9]' , `role` = '[1,2]' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 18:19:09' , `mtime` = '2023-05-20 22:57:40';
INSERT INTO `pan_role` SET `id` = 2 , `name` = 'ADMIN' , `title` = '管理员' , `menu` = '[1,2,4,12,14,5,6,7,13,11,9,10,15]' , `action` = '[10,1,9]' , `role` = '[2]' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-15 18:49:30' , `mtime` = '2023-05-18 15:20:59';

--
-- 表 `pan_rule` 结构
--

DROP TABLE IF EXISTS `pan_rule`;

CREATE TABLE `pan_rule` (`id` int(11) NOT NULL AUTO_INCREMENT,`module` varchar(100) NOT NULL,`controller` varchar(100) NOT NULL,`action` varchar(100) NOT NULL,`status` tinyint(1) DEFAULT 0,`dtime` timestamp NULL DEFAULT NULL,`atime` timestamp NULL DEFAULT current_timestamp(),`mtime` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- 表 `pan_rule` 数据
--
INSERT INTO `pan_rule` SET `id` = 1 , `module` = 'system' , `controller` = 'role' , `action` = 'index' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:44:54' , `mtime` = '2023-05-16 22:44:54';
INSERT INTO `pan_rule` SET `id` = 2 , `module` = 'system' , `controller` = 'account' , `action` = 'index' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:44:54' , `mtime` = '2023-05-16 22:44:54';
INSERT INTO `pan_rule` SET `id` = 3 , `module` = 'system' , `controller` = 'menu' , `action` = 'index' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:44:56' , `mtime` = '2023-05-16 22:44:56';
INSERT INTO `pan_rule` SET `id` = 4 , `module` = 'system' , `controller` = 'account' , `action` = 'field' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:45:02' , `mtime` = '2023-05-16 22:45:02';
INSERT INTO `pan_rule` SET `id` = 5 , `module` = 'system' , `controller` = 'account' , `action` = 'edit' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:45:09' , `mtime` = '2023-05-16 22:45:09';
INSERT INTO `pan_rule` SET `id` = 6 , `module` = 'system' , `controller` = 'role' , `action` = 'field' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:45:13' , `mtime` = '2023-05-16 22:45:13';
INSERT INTO `pan_rule` SET `id` = 7 , `module` = 'system' , `controller` = 'role' , `action` = 'edit' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:45:16' , `mtime` = '2023-05-16 22:45:16';
INSERT INTO `pan_rule` SET `id` = 8 , `module` = 'system' , `controller` = 'menu' , `action` = 'field' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:45:25' , `mtime` = '2023-05-16 22:45:25';
INSERT INTO `pan_rule` SET `id` = 9 , `module` = 'system' , `controller` = 'role' , `action` = 'rule' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:45:43' , `mtime` = '2023-05-16 22:45:43';
INSERT INTO `pan_rule` SET `id` = 10 , `module` = 'system' , `controller` = 'index' , `action` = 'index' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-16 22:48:06' , `mtime` = '2023-05-16 22:48:06';
INSERT INTO `pan_rule` SET `id` = 11 , `module` = 'system' , `controller` = 'log' , `action` = 'index' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 10:37:06' , `mtime` = '2023-05-17 10:37:06';
INSERT INTO `pan_rule` SET `id` = 12 , `module` = 'system' , `controller` = 'log' , `action` = 'clear' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 12:32:18' , `mtime` = '2023-05-17 12:32:18';
INSERT INTO `pan_rule` SET `id` = 13 , `module` = 'system' , `controller` = 'database' , `action` = 'index' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 13:30:32' , `mtime` = '2023-05-17 13:30:32';
INSERT INTO `pan_rule` SET `id` = 14 , `module` = 'system' , `controller` = 'database' , `action` = 'optimize' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 14:03:17' , `mtime` = '2023-05-17 14:03:17';
INSERT INTO `pan_rule` SET `id` = 15 , `module` = 'system' , `controller` = 'database' , `action` = 'repair' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 14:12:31' , `mtime` = '2023-05-17 14:12:31';
INSERT INTO `pan_rule` SET `id` = 16 , `module` = 'system' , `controller` = 'database' , `action` = 'backup' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 15:14:33' , `mtime` = '2023-05-17 15:14:33';
INSERT INTO `pan_rule` SET `id` = 17 , `module` = 'system' , `controller` = 'database' , `action` = 'progress' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 17:10:59' , `mtime` = '2023-05-17 17:10:59';
INSERT INTO `pan_rule` SET `id` = 18 , `module` = 'system' , `controller` = 'database' , `action` = 'delete' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 17:20:43' , `mtime` = '2023-05-17 17:20:43';
INSERT INTO `pan_rule` SET `id` = 19 , `module` = 'system' , `controller` = 'database' , `action` = 'restore' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 17:25:20' , `mtime` = '2023-05-17 17:25:20';
INSERT INTO `pan_rule` SET `id` = 20 , `module` = 'system' , `controller` = 'database' , `action` = 'config' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-17 18:54:25' , `mtime` = '2023-05-17 18:54:25';
INSERT INTO `pan_rule` SET `id` = 21 , `module` = 'system' , `controller` = 'menu' , `action` = 'add' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 09:54:06' , `mtime` = '2023-05-18 09:54:06';
INSERT INTO `pan_rule` SET `id` = 22 , `module` = 'system' , `controller` = 'account' , `action` = 'add' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 14:16:03' , `mtime` = '2023-05-18 14:16:03';
INSERT INTO `pan_rule` SET `id` = 23 , `module` = 'system' , `controller` = 'role' , `action` = 'delete' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 14:18:59' , `mtime` = '2023-05-18 14:18:59';
INSERT INTO `pan_rule` SET `id` = 24 , `module` = 'system' , `controller` = 'menu' , `action` = 'delete' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 14:19:20' , `mtime` = '2023-05-18 14:19:20';
INSERT INTO `pan_rule` SET `id` = 25 , `module` = 'system' , `controller` = 'account' , `action` = 'delete' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 15:01:30' , `mtime` = '2023-05-18 15:01:30';
INSERT INTO `pan_rule` SET `id` = 26 , `module` = 'system' , `controller` = 'menu' , `action` = 'edit' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 20:42:46' , `mtime` = '2023-05-18 20:42:46';
INSERT INTO `pan_rule` SET `id` = 27 , `module` = 'admin' , `controller` = 'scan' , `action` = 'index' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 22:21:34' , `mtime` = '2023-05-18 22:21:34';
INSERT INTO `pan_rule` SET `id` = 28 , `module` = 'admin' , `controller` = 'scan' , `action` = 'scan' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 22:56:55' , `mtime` = '2023-05-18 22:56:55';
INSERT INTO `pan_rule` SET `id` = 29 , `module` = 'admin' , `controller` = 'scan' , `action` = 'progress' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-18 23:43:04' , `mtime` = '2023-05-18 23:43:04';
INSERT INTO `pan_rule` SET `id` = 30 , `module` = 'admin' , `controller` = 'scan' , `action` = 'clear' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-19 08:45:28' , `mtime` = '2023-05-19 08:45:28';
INSERT INTO `pan_rule` SET `id` = 31 , `module` = 'admin' , `controller` = 'scan' , `action` = 'stop' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-19 09:03:13' , `mtime` = '2023-05-19 09:03:13';
INSERT INTO `pan_rule` SET `id` = 32 , `module` = 'admin' , `controller` = 'scan' , `action` = 'rate' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-19 12:39:03' , `mtime` = '2023-05-19 12:39:03';
INSERT INTO `pan_rule` SET `id` = 33 , `module` = 'system' , `controller` = 'log' , `action` = 'config' , `status` = 0 , `dtime` = NULL , `atime` = '2023-05-20 22:57:29' , `mtime` = '2023-05-20 22:57:29';
