<?php
namespace app\system\controller;

use app\BaseController;
use think\Facade\Db;

class Log extends BaseController
{
    public function index()
    {
        $page = input('post.page', 1);
        $rows = input('post.rows', 20);
        $datas = Db::name('log')->order('id', 'desc')->paginate(['list_rows' => strval($rows), 'var_page' => strval($page)])->each(function ($item, $key) {
            $item['user'] = Db::name('admin')->where('id', $item['uid'])->value('user');
            return $item;
        });

        return ['code' => 0, 'datas' => $datas];
    }

    public function delete()
    {
        $id = input('post.id');

        $data = ['status' => -1, 'dtime' => date('Y-m-d h:i:s', time())];
        $res = Db::name('admin')->whereIn('id', $id)->update($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function clear()
    {
        Db::execute("TRUNCATE `pan_log`");
        return ['code' => 0, 'message' => '已清空！'];
    }

    public function config()
    {
        if (request()->isPost()) {
            $enable = input('post.enable', 0);
            $result = input('post.result', 0);
            $login = input('post.login', 0);
            $action = input('post.action', []);

            $action = json_encode($action);

            $path = config_path() . 'record.php';
            $code = <<<EOT
<?php
return [
    'enable' => {$enable},
    'login' => {$login},
    'action' => {$action},
    'result' => {$result}
];
EOT;

            $res = file_put_contents($path, $code);
            if ($res) {
                return ['code' => 0, 'message' => '编辑配置成功！'];
            } else {
                return ['code' => 1, 'message' => '编辑配置失败！'];
            }
        } else {
            $datas = config('record');
            return ['code' => 0, 'datas' => $datas];
        }
    }

}
