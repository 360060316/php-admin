<?php
namespace app\system\controller;

use app\BaseController;

class Upload extends BaseController
{
    public function index()
    {
        $file = request()->file('image');
        $path = \think\facade\Filesystem::disk('public')->putFile('topic', $file);
        $datas['path'] = $path;
        $datas['md5'] = $file->md5();
        $datas['sha1'] = $file->sha1();
        return json(['code' => 0, 'datas' => $datas]);
    }

}
