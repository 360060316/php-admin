<?php
namespace app\system\controller;

use app\BaseController;
use thans\jwt\facade\JWTAuth;
use think\Facade\Db;

class Role extends BaseController
{
    private $role;
    private $roles;

    public function initialize()
    {
        $payload = JWTAuth::auth();
        $this->role = json_decode($payload['role'], true);
        $this->roles = Db::name('role')->whereIn('id', $this->role)->select();
    }

    public function index()
    {
        $page = input('post.page', 1);
        $rows = input('post.rows', 20);

        $ids = $this->getRoles($this->roles, 'role');

        $datas = Db::name('role')->withoutField('dtime,atime,mtime')->where('status', '<>', -1)->whereIn('id', $ids)->order('id', 'desc')->paginate(['list_rows' => strval($rows), 'var_page' => strval($page)])->each(function ($item, $key) {
            $role = Db::name('role')->field('title')->whereIn('id', json_decode($item['role'], true))->select();
            $item['permission'] = $role;
            return $item;
        });

        return ['code' => 0, 'datas' => $datas];
    }

    public function add()
    {
        $name = input('post.name');
        $title = input('post.title');
        $role = input('post.role');
        $status = input('post.status');

        $res = Db::name('role')->whereOr(['name' => $name, 'title' => $title])->find();

        if ($res) {
            return ['code' => 1, 'message' => '相同角色已存在！'];
        }

        $data = [
            'name' => $name,
            'title' => $title,
            'role' => json_encode($role),
            'status' => $status,
        ];

        $res = Db::name('role')->insert($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function edit()
    {
        $id = input('post.id');
        $name = input('post.name');
        $title = input('post.title');
        $role = input('post.role');
        $status = input('post.status');

        $data = [
            'name' => $name,
            'title' => $title,
            'role' => json_encode($role),
            'status' => $status,
        ];

        $res = Db::name('role')->where('id', $id)->update($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function delete()
    {
        $id = input('post.id');
        
        if (in_array($id, $this->role)) {
            return ['code' => 1, 'message' => '不能删除！'];
        }

        $data = ['status' => -1, 'dtime' => date('Y-m-d h:i:s', time())];
        $res = Db::name('role')->whereIn('id', $id)->update($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function field()
    {
        $id = input('post.id');
        $field = input('post.field');
        $value = input('post.value');

        $res = Db::name('role')->where('id', $id)->update([$field => $value]);

        if ($res) {
            return ['code' => 0, 'message' => '更改成功！'];
        } else {
            return ['code' => 1, 'message' => '更改失败！'];
        }
    }

    public function rule()
    {
        $datas = Db::name('rule')->field('module as label')->group('module')->order('module', 'ASC')->select()->each(function ($item, $key) {
            $item['isOne'] = true;
            $item['children'] = Db::name('rule')->field('controller as label')->group('controller')->where('module', $item['label'])->order('controller', 'ASC')->select()->each(function ($item, $key) {
                $item['isTwo'] = true;
                $item['children'] = Db::name('rule')->field('id,action as label')->where('controller', $item['label'])->order('action', 'ASC')->select();
                return $item;
            });
            return $item;
        });
        return ['code' => 0, 'datas' => $datas];
    }

    protected function getRoles($roles, $field)
    {
        $auth = [];
        foreach ($roles as $role) {
            $action = json_decode($role[$field], true);
            foreach ($action as $val) {
                $auth[] = $val;
            }
        }
        return array_unique($auth);
    }
}
