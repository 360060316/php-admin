<?php
namespace app\system\controller;

use app\BaseController;
use think\Facade\Db;

class Menu extends BaseController
{
    public function index()
    {
        $datas = Db::name('menu')->withoutField('dtime,atime,mtime')->where('status', '<>', -1)->order('sort', 'ASC')->select()->toArray();
        return ['code' => 0, 'datas' => $this->getTree($datas)];
    }

    public function add()
    {
        $pid = input('post.pid');
        $name = input('post.name');
        $title = input('post.title');
        $alias = input('post.alias');
        $path = input('post.path');
        $redirect = input('post.redirect');
        $component = input('post.component');
        $icon = input('post.icon');
        $aside = input('post.aside');
        $label = input('post.label');
        $breadcrumb = input('post.breadcrumb');
        $close = input('post.close');
        $cache = input('post.cache');
        $props = input('post.props');
        $sort = input('post.sort', 0);
        $status = input('post.status', 0);

        $where = ['name' => $name, 'path' => $path, 'title' => $title];
        $res = Db::name('menu')->whereOr($where)->find();

        if ($res) {
            return ['code' => 1, 'message' => '相同的菜单已存在！'];
        }

        $data = [
            'pid' => $pid,
            'name' => $name,
            'title' => $title,
            'alias' => $alias,
            'path' => $path,
            'redirect' => $redirect,
            'component' => $component,
            'icon' => $icon,
            'aside' => $aside ? 1 : 0,
            'label' => $label ? 1 : 0,
            'breadcrumb' => $breadcrumb ? 1 : 0,
            'close' => $close ? 1 : 0,
            'cache' => $cache ? 1 : 0,
            'props' => $props ? 1 : 0,
            'sort' => $sort,
            'status' => $status,
        ];
        //print_r($data);die;
        $res = Db::name('menu')->insert($data);
        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function edit()
    {
        $id = input('post.id');
        $pid = input('post.pid');
        $name = input('post.name');
        $title = input('post.title');
        $alias = input('post.alias');
        $path = input('post.path');
        $redirect = input('post.redirect');
        $component = input('post.component');
        $icon = input('post.icon');
        $aside = input('post.aside');
        $label = input('post.label');
        $breadcrumb = input('post.breadcrumb');
        $close = input('post.close');
        $cache = input('post.cache');
        $props = input('post.props');
        $sort = input('post.sort', 0);
        $status = input('post.status', 0);

        $data = [
            'pid' => $pid,
            'name' => $name,
            'title' => $title,
            'alias' => $alias,
            'path' => $path,
            'redirect' => $redirect,
            'component' => $component,
            'icon' => $icon,
            'aside' => $aside ? 1 : 0,
            'label' => $label ? 1 : 0,
            'breadcrumb' => $breadcrumb ? 1 : 0,
            'close' => $close ? 1 : 0,
            'cache' => $cache ? 1 : 0,
            'props' => $props ? 1 : 0,
            'sort' => $sort,
            'status' => $status,
        ];

        $res = Db::name('menu')->where('id', $id)->update($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function delete()
    {
        $id = input('post.id');
        $data = ['status' => -1, 'dtime' => date('Y-m-d h:i:s', time())];
        $res = Db::name('role')->whereIn('id', $id)->update($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function field()
    {
        $id = input('post.id');
        $field = input('post.field');
        $value = input('post.value');

        $res = Db::name('menu')->where('id', $id)->update([$field => $value]);

        if ($res) {
            return ['code' => 0, 'message' => '更改成功！'];
        } else {
            return ['code' => 1, 'message' => '更改失败！'];
        }
    }

    public function treeMenu()
    {
        $datas = Db::name('menu')->withoutField('dtime,atime,mtime')->where('status', '<>', -1)->select()->toArray();
        return ['code' => 0, 'datas' => $this->getTree($datas)];
    }

    protected function getTree($items, $pid = 0)
    {
        $tree = [];
        foreach ($items as $item) {

            if ($item['pid'] == $pid) {
                $temp = $this->getTree($items, $item['id']);
                if (!empty($temp)) {
                    $item['children'] = $temp;
                }
                $tree[] = $item;
            }

        }
        return $tree;
    }

}
