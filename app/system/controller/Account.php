<?php
namespace app\system\controller;

use app\BaseController;
use thans\jwt\facade\JWTAuth;
use think\Facade\Db;

class Account extends BaseController
{
    public function index()
    {
        $page = input('post.page', '1');
        $rows = input('post.rows', '20');

        $datas = Db::name('admin')->withoutField('pswd,dtime,atime')->where('status', '<>', -1)->order('id', 'desc')->paginate([
            'list_rows' => $rows,
            'var_page' => $page,
        ])->each(function ($item, $key) {
            $role = Db::name('role')->field('id,name,title')->whereIn('id', json_decode($item['role'], true))->select();
            $item['permission'] = $role;
            return $item;
        });

        return ['code' => 0, 'datas' => $datas];
    }

    public function add()
    {
        $role = input('post.role');
        $user = input('post.user');
        $pswd = input('post.pswd');
        $avatar = input('post.avatar');
        $phone = input('post.phone');
        $mail = input('post.mail');
        $status = input('post.status');

        $res = Db::name('admin')->where('user', $user)->find();

        if ($res) {
            return ['code' => 1, 'message' => '相同账号已存在！'];
        }

        $data = [
            'role' => json_encode($role),
            'user' => $user,
            'pswd' => md5('360060316' . $pswd),
            'avatar' => $avatar,
            'phone' => $phone,
            'mail' => $mail,
            'status' => $status,
        ];

        $res = Db::name('admin')->insert($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function edit()
    {
        $id = input('post.id');
        $role = input('post.role');
        $user = input('post.user');
        $avatar = input('post.avatar');
        $phone = input('post.phone');
        $mail = input('post.mail');
        $status = input('post.status');

        $data = [
            'role' => $role,
            'user' => $user,
            'avatar' => $avatar,
            'phone' => $phone,
            'mail' => $mail,
            'status' => $status,
        ];

        $res = Db::name('admin')->where('id', $id)->update($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function delete()
    {
        $payload = JWTAuth::auth();
        $id = input('post.id');

        if ($id == $payload['id']) {
            return ['code' => 1, 'message' => '不能删除！'];
        }

        $data = ['status' => -1, 'dtime' => date('Y-m-d h:i:s', time())];
        $res = Db::name('admin')->whereIn('id', $id)->update($data);

        if ($res) {
            return ['code' => 0, 'message' => '成功！'];
        } else {
            return ['code' => 1, 'message' => '失败！'];
        }
    }

    public function field()
    {
        $id = input('post.id');
        $field = input('post.field');
        $value = input('post.value');

        $res = Db::name('admin')->where('id', $id)->update([$field => $value]);

        if ($res) {
            return ['code' => 0, 'message' => '更改成功！'];
        } else {
            return ['code' => 1, 'message' => '更改失败！'];
        }
    }

}
