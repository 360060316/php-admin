<?php
namespace app\system\controller;

use app\BaseController;
use thans\jwt\facade\JWTAuth;
use think\Facade\Db;

class Index extends BaseController
{
    public function index()
    {
        $payload = JWTAuth::auth();
        $uid = $payload['id'];

        $user = Db::name('admin')->withoutField('pswd,status,dtime,atime,mtime')->find($uid);

        $roles = Db::name('role')->whereIn('id', json_decode($user['role'], true))->select();
        $ids = $this->getRoles($roles);
        $user['role'] = $this->getRolesName($roles);

        $menu = Db::name('menu')->withoutField('dtime,atime,mtime')->where('status', '<>', -1)->whereIn('id', $ids)->order('sort', 'ASC')->select()->toArray();
        $datas['user'] = $user;
        $datas['menu'] = $this->getAuthMenu($menu, 0);
        return ['code' => 0, 'datas' => $datas];
    }

    public function login()
    {
        if (request()->isPost() === false) {
            abort(404, '页面异常');
        }

        $username = input('post.username');
        $password = input('post.password');
        $password = md5('360060316' . $password);
        $res = Db::name('admin')->withoutField('dtime,atime,mtime')->where(['user' => $username, 'pswd' => $password])->find();

        if ($res) {
            if ($res['status'] != 0) {
                return ['code' => 1, 'message' => '账号已禁用！'];
            } else {
                $datas['token'] = JWTAuth::builder($res);
                return ['code' => 0, 'message' => '登录成功！', 'datas' => $datas];
            }
        } else {
            return ['code' => 1, 'message' => '登录失败！'];
        }

    }

    protected function getAuthMenu($data, $pid)
    {
        $menu = $tree = [];
        foreach ($data as $val) {
            $meta = [];
            $tree['name'] = $val['name'];
            $tree['path'] = $val['path'];
            $tree['component'] = $val['component'];

            if (empty($val['redirect'])) {
                unset($tree['redirect']);
            } else {
                $tree['redirect'] = $val['redirect'];
            }

            if (empty($val['alias'])) {
                unset($tree['alias']);
            } else {
                $tree['alias'] = $val['alias'];
            }

            if (!empty($val['title'])) {
                $meta['title'] = $val['title'];
            }

            if (!empty($val['icon'])) {
                $meta['icon'] = $val['icon'];
            }

            if (!empty($val['aside'])) {
                $meta['aside'] = $val['aside'];
            }

            if (!empty($val['breadcrumb'])) {
                $meta['breadcrumb'] = $val['breadcrumb'];
            }

            if (!empty($val['label'])) {
                $meta['label'] = $val['label'];
            }

            if (!empty($val['cache'])) {
                $meta['cache'] = $val['cache'];
            }

            if (!empty($val['close'])) {
                $meta['close'] = $val['close'];
            }

            if (!empty($val['props'])) {
                $meta['props'] = $val['props'];
            }

            $tree['meta'] = $meta;

            if ($val['pid'] == $pid) {
                $tree['children'] = $this->getAuthMenu($data, $val['id']);
                if ($tree['children'] == null) {
                    unset($tree['children']);
                }
                $menu[] = $tree;
            }
        }

        return $menu;
    }

    protected function getRoles($roles)
    {
        $auth = [];
        foreach ($roles as $role) {
            $action = json_decode($role['menu'], true);
            foreach ($action as $val) {
                $auth[] = $val;
            }
        }
        return array_unique($auth);
    }

    protected function getRolesName($roles)
    {
        $auth = [];
        foreach ($roles as $role) {
            $auth[] = $role['name'];
        }
        return $auth;
    }

}
